# WORKTOOLS

Utility scripts to manage work time.

    workon [<task>]  -- start working on a task (or show current one)
    workrep [<task>] [today|<regexp>] -- show (current) task logs
    worked [<task>]  -- edit (current) task logs [symlink to workrep]
    workoff [<log>]  -- stop working to current task, optionally updating log
    worklog [<log>]  -- update current task log [symlink to workoff]
    workleft         -- show a summary of pending projects (prepaid hours)

Work logs are kept in $LOGDIR env variable (defaults to ~/job/logs) and
are created on first checkout (workoff).
Ongoing task info is stored in $LOGDIR/.current.

You can optionally set an amount of prepaid hours for a task
starting the corresponding work log file with a line like
"PrepaidHours: 240 before 2015-10-30"; new log files are created
with a corresponding commented out template line for it.

Please report issues or send patches on: https://gitlab.com/strk/worktools

# REQUIREMENTS

    apt-get install lockfile-progs
